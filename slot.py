import random
from Decoder import ChineseNumDecoder, DateDecoder, TimeDecoder, PeriodDecoder, chiudauDecoder_Levenshtein_distance, chiudauDecoder_pinyin, chiudauDecoder_cha2pinyin2cha_Levenshtein
from datetime import timedelta, datetime


class Slot(object):
    def __init__(self, label, text=None, value=None, DEBUG=False):
        self.score = 0
        self.label = label
        self.text = text
        self.value = value
        if text is not None:
            self.decode(DEBUG=DEBUG)

    def decode(self, DEBUG=False):
        try:
            # text to value
            if self.label in ["MON", "金額"]:
                decode_tool = ChineseNumDecoder()
                self.value = round(decode_tool.transform(self.text), 2)

            elif self.label in ["DATE", "日期"]:
                decode_tool = DateDecoder()
                self.value = decode_tool.transform(self.text)

            elif self.label in ["CHA", "充值渠道"]:
                decode_tool = chiudauDecoder_cha2pinyin2cha_Levenshtein()
                self.value = decode_tool.transform(self.text, DEBUG=DEBUG)
                self.score = decode_tool.score

            elif self.label in ["TIME", "時間"]:
                decode_tool = TimeDecoder()
                self.value = decode_tool.transform(self.text)

            elif self.label in ["PER", "時間間隔"]:
                decode_tool = PeriodDecoder()
                self.value = decode_tool.transform(self.text)
        except KeyError as e:
            raise ValueError("KeyError: {}".format(e))
        except ValueError as e:
            raise ValueError("ValueError: {}".format(e))
        except UnboundLocalError as e:
            raise ValueError("UnboundLocalError: {}".format(e))
        except Exception as e:
            raise ValueError("Exception: {},{}".format(type(e),e))
    def encode(self):
        # value to text
        raise NotImplementedError

    def show(self):
        print("self.label = {}, self.text = {}, self.value = {}".format(self.label, self.text, self.value))



import random
from Decoder import ChineseNumDecoder, DateDecoder, TimeDecoder, PeriodDecoder, chiudauDecoder_Levenshtein_distance
from datetime import timedelta, datetime
from slot import Slot


class FakeSlotMoney(Slot):

    def __init__(self):
        self.label = "MON"
        self.fake_txt_arr = ["{}", "{}整", "{}左右", "{}多"]
        self.value = round(random.uniform(1, 50000), 2)
        Slot.__init__(self, label=self.label, text=self.encode(), value=self.value)
        self.decode()

    def encode(self):
        self.value = self.value if random.randint(0, 1) else int(self.value)
        fake_value = self.tool_num2Chinese(self.value)
        return random.choice(self.fake_txt_arr).format(fake_value)

    def tool_num2Chinese(self, value: float):
        if value >= 10000:
            value = round(value / 10000, 2)
            txt_arr = ["{}万", "{}w"]
            txt = random.choice(txt_arr).format(value)

        elif value >= 1000:
            value = round(value / 1000, 2)
            txt_arr = ["{}千", "{}仟"]
            txt = random.choice(txt_arr).format(value)
        else:
            txt_arr = ["{}元", "{}块"]
            txt = random.choice(txt_arr).format(value)
        return txt


class FakeSlotGold(Slot):

    def __init__(self):
        self.label = "GOLD"
        self.fake_txt_arr = ["{}币", "{}金币"]
        self.value = round(random.uniform(0.1, 50000),2)
        Slot.__init__(self, label=self.label, text=self.encode(), value=self.value)
        self.decode()

    def encode(self):
        self.value = self.value if random.randint(0, 1) else int(self.value)
        fake_value = self.tool_num2Chinese(self.value)
        return random.choice(self.fake_txt_arr).format(fake_value)

    def tool_num2Chinese(self, value: float):
        if value >= 10000:
            value = round(value / 10000, 2)
            txt_arr = ["{}万", "{}w"]
            txt = random.choice(txt_arr).format(value)

        elif value >= 1000:
            value = round(value / 1000, 2)
            txt_arr = ["{}千", "{}仟"]
            txt = random.choice(txt_arr).format(value)
        else:
            txt_arr = ["{}元", "{}块"]
            txt = random.choice(txt_arr).format(value)
        return txt


class FakeSlotDate(Slot):

    def __init__(self):
        self.label = "DATE"
        self.year = random.randint(1900, 2100)
        self.month = random.randint(1, 12)
        self.day = random.randint(1, 28)
        self.value = datetime(year=self.year, month=self.month, day=self.day)
        Slot.__init__(self, label=self.label, text=self.encode(), value=self.value)
        self.decode()

    def encode(self):
        if random.randint(0,1):
            re = "{}年{}月{}日".format(self.year, self.month, self.day)
        else:
            re = "{}年{}月{}日".format(self.tool_num2Chinese(self.year), self.tool_num2Chinese(self.month), self.tool_num2Chinese(self.day))

        return re

    def tool_num2Chinese(self, value: float):
        self.numerals = {0:'一', 1:'一', 2:'二', 3:'三', 4:'四', 5:'五', 6:'六', 7:'七', 8:'八', 9:'九'}
        arr = ""
        for c in str(value):
            arr+=self.numerals[int(c)]

        return arr


class FakeSlotCHA(Slot):

    def __init__(self):
        self.label = "充值渠道"
        self.keyword = {
            "支付宝定额": "支付宝定额",
            "支付宝": "支付宝",
            "云闪付": "云闪付",
            "尊享闪付": "尊享闪付",
            "微信": "微信",
            "花呗": "花呗",
            "充值卡": "充值卡",
            "银行卡": "银行卡"
        }
        self.value = random.choice([i for i in self.keyword.values()])
        Slot.__init__(self, label=self.label, text=self.encode(), value=self.value)

    def encode(self):
        if random.randint(0,1):
            re = self.value
        else:
            pos = random.randint(0, len(self.value))
            re = self.value[:pos]+random.choice(["o", "x", ""])+self.value[pos:]
        if random.randint(0, 1):
            pos = random.randint(0, len(self.value))
            re = self.value[:pos-1 if pos > 0 else 0]+self.value[pos:]

        return re


class FakeSlotPeriod(Slot):

    def __init__(self):
        self.label = "時間間隔"
        self.value = "{hour}:{minute}:00".format(hour=random.randint(0, 5000), minute=random.randint(0, 55))
        Slot.__init__(self, label=self.label, text=self.encode(), value=self.value)
        self.decode()

    def encode(self):
        raw = self.value.split(":")
        if random.randint(0,1) and int(raw[0]) > 24:
            if random.randint(0, 1):
                re = "{}天{}小時{}分".format(int(raw[0]) // 24, int(raw[0]) % 24, raw[1])
            else:
                re = "{}天{}小時{}分".format(self.NumInt2ChineseInt(int(raw[0]) // 24), self.NumInt2ChineseInt(int(raw[0]) % 24), raw[1])
        else:
            if random.randint(0, 1):
                re = "{}小時{}分".format(raw[0],raw[1])
            else:
                re = "{}小時{}分".format(self.NumInt2ChineseInt(raw[0]), self.NumInt2ChineseInt(raw[1]))
        return re

    @classmethod
    def NumInt2ChineseInt(self,num) -> str:
        units = ["", "十", "百", "千", "萬"]
        ChineseNum = {0: '零', 1: '一', 2:'二', 3:'三', 4:'四', 5:'五', 6:'六', 7:'七', 8:'八', 9:'九'}
        num = str(num)
        if len(num) > len(units):
            return num
        pointer = len(num)-1
        chinese_str = ""
        for i in num:
            chinese_str += ChineseNum[int(i)] + units[pointer]
            pointer-=1

        return chinese_str


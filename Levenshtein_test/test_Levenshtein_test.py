import logging
import os
import unittest
import datetime
import csv
import re
from datetime import date, timedelta, datetime

from slot import FakeSlotGold, FakeSlotMoney, Slot
from Levenshtein_test.mock_Decoder import chiudauDecoder

class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        output=[]
        # 開啟 CSV 檔案
        with open('./test_sample.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile,delimiter='\t')
            # 以迴圈輸出每一列
            for idx, r in enumerate(rows):
                if idx == 0: continue
                row = r[0].split(",")
                label = row[0]
                text = row[1]
                ans = row[2]
                convented_value = [text, ans]
                if label == "充值渠道":
                    a = chiudauDecoder()

                    #
                    convented = a.transform_Levenshtein_distance(text,threshold=0.6)
                    if convented[1] == ans:
                        convented_value.append("o")
                    else:
                        convented_value.append("x")
                    convented_value.extend(convented)

                    #
                    convented = a.transform_Levenshtein_ratio(text,threshold=0.6)
                    if convented[1] == ans:
                        convented_value.append("o")
                    else:
                        convented_value.append("x")
                    convented_value.extend(convented)
                    #
                    convented = a.transform_Levenshtein_jaro(text,threshold=0.6)
                    if convented[1] == ans:
                        convented_value.append("o")
                    else:
                        convented_value.append("x")
                    convented_value.extend(convented)

                    #
                    output.append(convented_value)
        # 開啟輸出的 CSV 檔案
        with open('compared.csv', 'w', newline='') as csvfile:
            # 建立 CSV 檔寫入器
            writer = csv.writer(csvfile)

            # 寫入一列資料
            writer.writerow(["text","ans", "distance","","","ratio","","","jaro","",""])

            for r in output:
                # 寫入另外幾列資料
                writer.writerow(r)


    def test_SlotDate(self):
       pass
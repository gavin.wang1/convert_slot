from datetime import date, timedelta, datetime
import re
import Levenshtein

class Decoder(object):

    def transform(self,text):
        raise NotImplementedError

class chiudauDecoder(Decoder):
    def __init__(self):
        self.keyword = {
            "支付宝定额":"支付宝定额",
            "支付宝":"支付宝",
            "云闪付":"云闪付",
            "尊享闪付":"尊享闪付",
            "微信":"微信",
            "花呗":"花呗",
            "充值卡":"充值卡",
            "银行卡":"银行卡"
        }

    def transform_Levenshtein_distance(self, slot, threshold=None):
        Best_one = None
        for key, val in self.keyword.items():
            score = Levenshtein.distance(key, slot)
            score = 1- round(score/(len(slot)+len(key)),2)
            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]

        if threshold is not None and Best_one[0] <= threshold:
            return [Best_one[0], "UNKNOWN"]
        return Best_one

    def transform_Levenshtein_ratio(self, slot, threshold=None):
        Best_one = None
        for key, val in self.keyword.items():
            score = round(Levenshtein.ratio(key, slot), 2)
            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]
        if threshold is not None and Best_one[0] <= threshold:
            return [Best_one[0], "UNKNOWN"]
        return Best_one

    def transform_Levenshtein_jaro(self, slot, threshold=None):
        Best_one = None
        for key, val in self.keyword.items():
            score = round(Levenshtein.jaro(key, slot), 2)

            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]
        if threshold is not None and Best_one[0] <= threshold:
            return [Best_one[0], "UNKNOWN"]
        return Best_one
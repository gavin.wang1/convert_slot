from datetime import date, timedelta, datetime
from pypinyin import pinyin, lazy_pinyin
import re
import Levenshtein


class Decoder(object):

    def transform(self,text):
        raise NotImplementedError

    def cn2digit(self, chars_cn):

        common_used_numerals = {'角': 0.5, '毛': 0.5, '零': 0, '一': 1, '二': 2, '两':2,'兩':2,'俩':2, '三': 3, '四': 4, '五': 5,
                                '六': 6, '七': 7,'八': 8, '九': 9,'十': 10, "上": 1, '0': 0, '1': 1, '2': 2, '3': 3,
                                '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '百': 100, '千': 1000, '万': 10000,
                                '亿': 100000000, '元': 1}
        if "半" in str(chars_cn):
            chars_cn = chars_cn.replace("半", ".5")
        if not chars_cn:
            return 0
        try:
            return float(chars_cn)
        except:
            pass
        for unit in ['亿', '万', '千', '百', '十', '元']:
            if unit in chars_cn:
                ps = chars_cn.split(unit)
                lp = self.cn2digit(ps[0])
                if lp == 0 and unit != '元':
                    lp = 1

                rp = self.cn2digit(ps[1])
                return lp * common_used_numerals[unit] + rp
        return common_used_numerals[chars_cn[-1]]


class ChineseNumDecoder(Decoder):
    def __init__(self):
        self.common_used_numerals = {'角':0.5,'毛':0.5,'零': 0, '一': 1, '二': 2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9,
                                     '十': 10,"上":1,
                                     '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
                                     '百': 100, '千': 1000, '万': 10000, '亿': 100000000, '元': 1}

        self.numerals = {'零': 0, '一': 1, '二': 2, '兩': 2, '两': 2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9}
        self.units = {'十': '元', '百': '十', '千': '百', '万': '千', '亿': '千万'}
        self.chinese_map = str.maketrans('o〇壹兩两贰叁肆伍陆柒捌玖拾佰仟kK萬wWeE块圆钱整快', '0零一二二二三四五六七八九十百千千千万万万亿亿元元元元元')

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
            }
            text = args[0]
            return cases[text] if text in cases else func(self, text)

        return func_wrapper

    @exception_case
    def transform(self, chars_cn):

        original_input = chars_cn
        # remove adj
        if isinstance(chars_cn, str):
            chars_cn = chars_cn.replace('左右', '')
            chars_cn = chars_cn.replace('多', '')
            chars_cn = chars_cn.replace('几', '')
            chars_cn = chars_cn.replace('币', '')
            chars_cn = chars_cn.replace('金币', '')
            chars_cn = chars_cn.replace('金', '')
            chars_cn = chars_cn.replace('来', '')
            chars_cn = chars_cn.replace('+', '')
            chars_cn = chars_cn.replace('点', '.')
            chars_cn = chars_cn.translate(self.chinese_map)

        try:

            return float(chars_cn if '元' not in chars_cn else chars_cn.replace('元',''))
        except ValueError as e:
            pass#print("error",e)
        if '.' in chars_cn and chars_cn[-1] not in self.units:  # #4.33 #四点三十三 #十三点十三
            try:
                chars_cn_arr = chars_cn.split('.')
                left_part = self.cn2digit(chars_cn_arr[0])
                right_part = self.cn2digit(chars_cn_arr[1])
                right_part = '0.' + str(int(right_part))
                return left_part + float(right_part)
            except ValueError as e:
                pass#print("error",e)
        if len(chars_cn) == 0:
            return 0
        if len(chars_cn) > 2:
            if chars_cn[-1] in self.numerals:
                if chars_cn[-2] in self.units.keys():
                    chars_cn = chars_cn + self.units[chars_cn[-2]]

        return float(self.cn2digit(chars_cn))


class DateDecoder(Decoder):
    def __init__(self):
        self.numerals = {'零': 0, '一': 1, '二': 2, '兩': 2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9}
        self.units = {'十': '元', '百': '十', '千': '百', '万': '千', '亿': '千万'}
        self.common_used_numerals = {'零': 0, '一': 1, '二': 2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9,
                                     '十': 10,
                                     '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
                                     '百': 100, '千': 1000, '万': 10000, '亿': 100000000, '元': 1}
        self.weekenddat={
            "一":0,
            "二":1,
            "三":2,
            "四":3,
            "五":4,
            "六":5,
            "日":6,
            "天":6,
        }

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
            }
            text = args[0]
            return cases[text] if text in cases else func(self, text)

        return func_wrapper

    @exception_case
    def transform(self, slot: str):
        if len(slot) == 8 and slot.isdigit():
            re = datetime(year=int(slot[0:4]), month=int(slot[4:6]), day=int(slot[6:8]))
        elif '今' in slot:
            re = date.today()
        elif '昨' in slot:
            re = (date.today() - timedelta(days=1))
        elif '前' in slot:
            re = (date.today() - timedelta(days=2))
        elif '次' in slot:
            re = (date.today() + timedelta(days=1))
        elif '明' in slot:
            re = (date.today() + timedelta(days=1))
        elif "上週" in slot or "上星期" in slot or "上禮拜" in slot:
            if slot in ["上週","上星期","上禮拜"]:
                re = (date.today() - timedelta(days=7))
            else:
                slot = slot.replace("上週","").replace("上星期","").replace("上禮拜","")
                last_weekday_today = date.today() - timedelta(days=7)
                re = last_weekday_today - timedelta(days=(last_weekday_today.weekday() - self.weekenddat[slot]))

        else:
            dat_ret, tmpQur = self.get_date_re(slot)
            if dat_ret:
                return dat_ret[0]
            else:
                if '年' in slot:
                    year, slot = slot.split('年')
                    year = int(self.chineseYear2Decode(year))
                else:
                    year = date.today().year
                optional_unit = slot[-1]
                slot = slot.replace('_', '-').replace('.', '-').replace('月', '-').replace('日', '').replace('号', '').replace('，', '-')
                slot = slot.replace('--', '-')
                slot = slot.strip('-')
                if len(slot.split('-')) == 1 and len(slot) == 4:
                    year = int(slot)
                    month = 1
                    day = 1
                elif len(slot.split('-')) == 1 and (optional_unit == '日' or optional_unit == '号'):
                    year = datetime.now().year
                    month = datetime.now().month
                    day = int(slot)
                elif len(slot.split('-')) == 2:
                    if len(slot.split('-')[0]) == 4:  # first is year
                        year, month = slot.split('-')
                        year = int(self.cn2digit(year))
                        month = int(self.cn2digit(month))
                        day = datetime.now().day
                    else:
                        month, day = slot.split('-')
                        month = int(self.cn2digit(month))
                        day = int(self.cn2digit(day))
                elif len(slot.split('-')) == 3:
                    year, month, day = slot.split('-')
                    year = int(self.cn2digit(year))
                    month = int(self.cn2digit(month))
                    day = int(self.cn2digit(day))
                re = datetime(year=year, month=month, day=day)

        return re.strftime("%Y-%m-%d")

    def get_date_re(self, query):
        lst_date = []
        date_all = re.findall(r'(\d{4}-\d{1,2}-\d{1,2})', query)  # rule
        for item in date_all:
            if self.validate_date(item):
                lst_date.append(datetime.strptime(item, '%Y-%m-%d').strftime("%Y-%m-%d"))  # return format
                query = query.replace(item, '#get_date_re#')
        return lst_date, query

    def validate_date(self, date_text):
        try:
            # 兩邊都正規劃到 2019-3-1
            if datetime.strptime(date_text, '%Y-%m-%d').strftime("%Y-%m-%d").replace('-0', '-') != date_text.replace(
                    '-0',
                    '-'):
                raise ValueError
            return True
        except ValueError:
            # raise ValueError("错误是日期格式或日期,格式是年-月-日")
            return False

    def chineseYear2Decode(self,cha:str):
        try:
            return float(cha)
        except ValueError:
            pass
        if len(cha) == 0:
            return 0
        if len(cha) > 2:
            new_row = ""
            for c in cha:
                if c in self.numerals:
                    new_row+=str(self.numerals[c])
                elif c in ["千","零"]:
                    new_row += "0"

        return int(new_row)


class TimeDecoder(Decoder):
    def __init__(self):
        self.numerals = {'零': 0, '一': 1, '二': 2, '兩': 2, "两":2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9, "十": 10}

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
                "上午": "09:00",
                "早上": "09:00",
                "早": "09:00",
                "中午": "12:00",
                "晚上": "18:00",
                "晚": "18:00",
                "下午": "15:00",
                "傍晚": "18:00",
            }
            text = args[0]

            return cases[text] if text in cases else func(self, text)
        return func_wrapper

    @exception_case
    def transform(self, slot:str):
        if '前' in slot:
            supportDecoder = PeriodDecoder()
            temp_time = supportDecoder.transform(slot.replace("前", ""))
            temp_time_arr = temp_time.split(":")
            return (datetime.now()-timedelta(hours=int(temp_time_arr[0]),minutes=int(temp_time_arr[1]))).strftime("%H:%M")
        else:
            if isinstance(slot, str):
                slot = slot.replace('间', '')
                if '小时' not in slot:
                    slot = slot.replace('时', '')
                slot = slot.replace('几', '')
                slot = slot.replace('多', '')
                slot = slot.replace('昨', '')
                slot = slot.replace('今', '')
                slot = slot.replace('明', '')
                slot = slot.replace('左右', '')

            hour_delta = 0
            minute_delta = 0
            now = datetime.now()
            TimePer = None
            if '刚' in slot:
                return now.strftime("%H:%M")
            if ('凌晨' in slot):
                hour_delta = 24
                TimePer = "凌晨"
            if ('晚上' in slot) or ('下午' in slot) or ('傍晚' in slot) or ('中午' in slot):
                hour_delta = 12
                TimePer = "晚上"
            if ('點半' in slot) or ('点半' in slot):
                minute_delta = 30
            if '分钟前' in slot:
                slot = slot.replace('分钟前', '')
                minute_delta = int(self.cn2digit(slot))
                minute = now.minute
                hour = now.hour
                if minute - minute_delta > 0:
                    return now.replace(minute=minute - minute_delta).strftime("%H:%M")
                else:
                    return now.replace(hour=hour - 1, minute=minute - minute_delta + 60).strftime("%H:%M")
            slot = slot.replace('晚上', '').replace('下午', '').replace('傍晚', '').replace('中午', '')
            slot = slot.replace('早上', '').replace('上午', '').replace('半', '').replace('凌晨', '')
            slot = slot.replace('點', '點').replace('点', '點').replace('.', '點').replace('：', ':').replace('-', ':').replace('分', '分')
            slot_arr = [p for p in re.split("點|点|\:|\：|\.|\-|分", slot) if p != ""]

            if len(slot_arr) == 3:
                hour, minute, sec = slot_arr
            elif len(slot_arr) == 2:
                hour, minute = slot_arr
            elif len(slot_arr) == 1:
                if "分" in slot:
                    if hour_delta == 24:
                        hour = 0
                    else:
                        hour = int(datetime.now().strftime("%H"))
                    minute = slot_arr[0]
                else:
                    hour = slot_arr[0]
                    minute = 0
            else:
                hour, minute = 0, 0
            hour = int(self.cn2digit(hour)) + hour_delta
            if hour > 24 and TimePer=="晚上":
                # 晚上20点36分 -> 20 + 12(”晚上" hour_delta) -> 32
                hour = hour - 12
            elif hour > 24 and TimePer=="凌晨":
                # 凌晨1点36分 -> 24 + 1(”凌晨" hour_delta) -> 25
                hour = hour - 24
            elif hour == 24:
                # 24点36分
                hour = 0

            minute = int(self.cn2digit(minute)) + minute_delta

            return (datetime.now().replace(hour=hour, minute=minute)).strftime("%H:%M")


class PeriodDecoder(Decoder):
    def __init__(self):
        self.numerals = {'零': 0, '一': 1, '二': 2, '兩': 2, '两':2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9, "十": 10,"半":0.5}
        self.units={
            "天":24*60,
            "夜":12*60,
            "上午":12*60,
            "晚上":12*60,
            "几天":24*60,
            "多天":24*60,
            "日": 24 * 60,
            "星期": 7 * 24 * 60,
            "小時":60,
            "小时":60,
            "钟":60,
            "分鐘":1,
            "分钟":1,
            "分":1,
            "月":30*24*60,
            "年":30*24*60*12
        }

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
                "几天": timedelta(days=1),
                "几月": timedelta(days=30),
                "几年": timedelta(days=360),
                "几分钟": timedelta(minutes=1),
            }
            text = args[0]

            return self.output_format(cases[text]) if text in cases else func(self, text)
        return func_wrapper

    @exception_case
    def transform(self, slot):
        # remove adj
        if isinstance(slot, str):
            slot = slot.replace('好几', '一')
            slot = slot.replace('好多', '一')
            slot = slot.replace('几个', '一')
            slot = slot.replace('好', '')
            slot = slot.replace('個', '')
            slot = slot.replace('个', '')
            slot = slot.replace('好', '')
            slot = slot.replace('几', '')
            slot = slot.replace('多', '')
        # 2天
        # 3小時
        substr = self.findall(slot)
        time_sum = timedelta()
        for word, w_len in substr:
            unit = word[-w_len:]
            try:
                num = int(word[:-w_len])
            except:
                # stop
                num = self.cn2digit(word[:-w_len])
            temp_time_minutes = num*int(self.units[unit])
            time_sum += timedelta(minutes=int(temp_time_minutes))

        return self.output_format(time_sum)

    def findall(self,sentence:str):
        units_word = "|".join(self.units.keys())
        words = re.findall(units_word, sentence)
        pos_arr = []
        sentence_arr = []
        for w in words:
            pos = sentence.index(w)
            pos_arr.append({"pos": pos, "word_len": len(w)})

        for idx, p in enumerate(pos_arr):
            if idx == 0:
                sentence_arr.append([sentence[:p["pos"]+p["word_len"]], p["word_len"]])
            else:
                sentence_arr.append([
                    sentence[pos_arr[idx-1]["pos"]+pos_arr[idx-1]["word_len"]:p["pos"]+p["word_len"]],
                    p["word_len"]
                                     ])
        #print(sentence_arr)
        return sentence_arr


    @classmethod
    def output_format(cls, per:timedelta) -> str:
        if "days" not in str(per) and "day" not in str(per):
            return str(per)
        per = str(per).replace("days", "").replace("day", "").strip()
        per_arr = per.split(",")
        day = int(per_arr[0])
        hours = int(per_arr[1].split(":")[0])
        minute = int(per_arr[1].split(":")[1])
        minute = "%02d" % minute
        return "{}:{}:00".format(day*24+hours, minute)




class chiudauDecoder_Levenshtein_distance(Decoder):
    def __init__(self):
        self.score = 0
        self.keyword = {
            "支付宝定额": "支付宝定额",
            "定额": "支付宝定额",
            "支付宝": "支付宝",
            "云闪付": "云闪付",
            "尊享闪付": "尊享闪付",
            "尊享": "尊享闪付",
            "闪付": "尊享闪付",
            "闪充": "尊享闪付",
            "尊贵": "尊享闪付",
            "代理充值": "尊享闪付",
            "微信": "微信",
            "微信定额": "微信定额",
            "花呗": "花呗",
            "充值卡": "充值卡",
            "银行卡": "银行",
            "银联": "银行"
        }

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
                "花贝": "花呗",
                "尊亨": "尊享闪付"
            }
            text = args[0]

            return cases[text] if text in cases else func(self, text, DEBUG=kwargs['DEBUG'])

        return func_wrapper

    @exception_case
    def transform(self, slot, threshold=0.5, DEBUG=False):  # _Levenshtein_ratio
        Best_one = None
        for key, val in self.keyword.items():

            score = round(Levenshtein.ratio(key, slot), 2)
            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]
        self.score = Best_one[0]
        if not DEBUG:
            if (threshold is not None and Best_one[0] <= threshold) or len(slot) == 1:
                raise ValueError("{} is UNKNOWN".format(slot))

        return Best_one[1]


class chiudauDecoder_cha2pinyin2cha_Levenshtein(Decoder):
    def __init__(self):
        self.score = 0
        self.keyword = {
            "支付宝定额": "支付宝定额",
            "定额": "支付宝定额",
            "支付宝": "支付宝",
            "云闪付": "云闪付",
            "尊享闪付": "尊享闪付",
            "尊享": "尊享闪付",
            "闪付": "尊享闪付",
            "闪充": "尊享闪付",
            "尊贵": "尊享闪付",
            "代理充值": "尊享闪付",
            "微信": "微信",
            "微信定额": "微信定额",
            "花呗": "花呗",
            "充值卡": "充值卡",
            "银行卡": "银行",
            "银联": "银行"
        }
        self.pinyin2cha={}

        for key,val in self.keyword.items():
            for c in key:
                c_yin = lazy_pinyin(c)[0]
                if c_yin not in self.pinyin2cha:
                    self.pinyin2cha[c_yin] = c
            for c in val:
                c_yin = lazy_pinyin(c)[0]
                if c_yin not in self.pinyin2cha:
                    self.pinyin2cha[c_yin] = c

    def cha2pinyin2cha(self, word:str):
        re_word = ""
        for c in word:
            c_yin = lazy_pinyin(c)[0]
            if c_yin not in self.pinyin2cha:
                self.pinyin2cha[c_yin] = c
            re_word += self.pinyin2cha[c_yin]
        return re_word

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
                "花贝": "花呗",
                "尊亨": "尊享闪付"
            }
            text = args[0]

            return cases[text] if text in cases else func(self, text, DEBUG=kwargs['DEBUG'])

        return func_wrapper


    @exception_case
    def transform(self, slot, threshold=0.5, DEBUG=False):  # _Levenshtein_ratio
        Best_one = None
        slot = self.cha2pinyin2cha(slot)
        #print(slot)
        for key, val in self.keyword.items():
            key = self.cha2pinyin2cha(key)
            score = round(Levenshtein.ratio(key, slot), 2)
            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]
        self.score = Best_one[0]
        if not DEBUG:
            if (threshold is not None and Best_one[0] <= threshold) or len(slot) == 1:
                raise ValueError("{} is UNKNOWN".format(slot))
        return Best_one[1]


class chiudauDecoder_pinyin(Decoder):
    def __init__(self):
        self.score = 0
        self.keyword = {
            "支付宝定额": "支付宝定额",
            "定额": "支付宝定额",
            "支付宝": "支付宝",
            "云闪付": "云闪付",
            "尊享闪付": "尊享闪付",
            "尊享": "尊享闪付",
            "闪付": "尊享闪付",
            "闪充": "尊享闪付",
            "尊贵": "尊享闪付",
            "代理充值": "尊享闪付",
            "微信": "微信",
            "微信定额": "微信定额",
            "花呗": "花呗",
            "充值卡": "充值卡",
            "银行卡": "银行",
            "银联": "银行"
        }

    def exception_case(func: classmethod):
        def func_wrapper(self, *args, **kwargs):
            cases = {
                "花贝": "花呗",
                "尊亨": "尊享闪付"
            }
            text = args[0]

            return cases[text] if text in cases else func(self, text, DEBUG=kwargs['DEBUG'])

        return func_wrapper


    @exception_case
    def transform(self, slot, threshold=0.7, DEBUG=False):  # _Levenshtein_ratio
        Best_one = None
        for key, val in self.keyword.items():
            pinyin_slot = "".join(lazy_pinyin(slot))
            pinyin_key = "".join(lazy_pinyin(key))
            # score = round(Levenshtein.ratio(pinyin_key, pinyin_slot), 2)
            score = round(((len(pinyin_key)+len(pinyin_slot))-Levenshtein.distance(pinyin_key, pinyin_slot))/(len(pinyin_key)+len(pinyin_slot)), 2)

            if Best_one is None:
                Best_one = [score, val]
            if score > Best_one[0]:
                Best_one = [score, val]
        self.score = Best_one[0]
        if not DEBUG:
            if (threshold is not None and Best_one[0] <= threshold) or len(slot) == 1:
                raise ValueError("{} is UNKNOWN".format(slot))

        return Best_one[1]
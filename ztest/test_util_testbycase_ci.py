import logging
import os
import unittest
import datetime
import csv
import re
from datetime import date, timedelta, datetime

from slot import Slot
from FakeSlot import FakeSlotGold, FakeSlotMoney

class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        self.localhost = os.path.dirname(os.path.abspath(__file__))
        pass


    def test_SlotTime(self):

        replace_words ={
            "前半小时": (datetime.now()-timedelta(minutes=30)).strftime("%H:%M"),
            "5分钟前": (datetime.now()-timedelta(minutes=5)).strftime("%H:%M"),
            "刚刚": (datetime.now()).strftime("%H:%M"),
            "五十分": (datetime.now().replace(minute=50)).strftime("%H:%M"),
        }
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/test_csvDoc/time.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile, delimiter=',')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                ans = row[0]
                if row[1] in replace_words:
                    ans = replace_words[row[1]]
                TimeObj = Slot(label="TIME", text=row[1])
                #print(row[0], row[1])
                self.assertEqual(TimeObj.value, ans)

    def test_SlotCha(self):

        replace_words ={
            "前半小时": (datetime.now()-timedelta(minutes=30)).strftime("%H:%M"),
            "5分钟前": (datetime.now()-timedelta(minutes=5)).strftime("%H:%M"),
            "刚刚": (datetime.now()).strftime("%H:%M"),
            "五十分": (datetime.now().replace(minute=50)).strftime("%H:%M"),
        }
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/test_csvDoc/cha.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile, delimiter=',')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                ans = row[0]
                if row[1] in replace_words:
                    ans = replace_words[row[1]]

                try:
                    TimeObj = Slot(label="CHA", text=row[1])
                    #print(row[1], TimeObj.value, ans)
                    self.assertEqual(TimeObj.value, ans)
                except ValueError as e:
                    pass#self.assertEqual(str(e), f"ValueError: {row[1]} is UNKNOWN")

    def test_SlotPer(self):

        replace_words ={
            "前半小时": (datetime.now()-timedelta(minutes=30)).strftime("%H:%M"),
            "5分钟前": (datetime.now()-timedelta(minutes=5)).strftime("%H:%M"),
            "刚刚": (datetime.now()).strftime("%H:%M"),
            "五十分": (datetime.now().replace(minute=50)).strftime("%H:%M"),
        }
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/test_csvDoc/per.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile, delimiter=',')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                #print(row)
                ans = row[0]
                if row[1] in replace_words:
                    ans = replace_words[row[1]]
                TimeObj = Slot(label="PER", text=row[1])
                self.assertEqual(TimeObj.value, ans)

    def test_SlotDate(self):

        replace_words ={
            "2019.6.": (datetime(year=2019,month=6,day=int(datetime.now().strftime("%d")))).strftime("%Y-%m-%d"),
            "前天": (datetime.now()-timedelta(days=2)).strftime("%Y-%m-%d"),
            "昨天": (datetime.now()-timedelta(days=1)).strftime("%Y-%m-%d"),
            "昨晚": (datetime.now()-timedelta(days=1)).strftime("%Y-%m-%d"),
            "明天": (datetime.now()+timedelta(days=1)).strftime("%Y-%m-%d"),
            "次日": (datetime.now()+timedelta(days=1)).strftime("%Y-%m-%d"),
            "今天": datetime.now().strftime("%Y-%m-%d"),
        }
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/test_csvDoc/date.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile, delimiter=',')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                ans = row[0]
                if row[1] in replace_words:
                    ans = replace_words[row[1]]
                TimeObj = Slot(label="DATE", text=row[1])
                #print(row[0], row[1])
                self.assertEqual(TimeObj.value, ans)

    def test_SlotMoney(self):

        replace_words ={
        }
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/test_csvDoc/money.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile, delimiter=',')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                ans = float(row[0])
                if row[1] in replace_words:
                    ans = replace_words[row[1]]
                TimeObj = Slot(label="MON", text=row[1])
                #print(row[0], row[1])
                self.assertEqual(TimeObj.value, ans)

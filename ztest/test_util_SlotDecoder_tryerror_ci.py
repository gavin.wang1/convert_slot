import logging
import os
import unittest
import datetime
from datetime import date, timedelta, datetime

from FakeSlot import FakeSlotGold, FakeSlotMoney, Slot
from slot import Slot
from Decoder import PeriodDecoder

class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        today = date.today() - timedelta(days=1)
        offset = (today.weekday() - 2) % 7
        last_wednesday = today - timedelta(days=offset)
        self.lastweek_3 = last_wednesday
        pass

    def test_SlotDate(self):
        #
        try:
            Slot(label="DATE", text="錯誤範例")
        except Exception as e:
            self.assertEqual(str(e), str("ValueError: invalid literal for int() with base 10: '錯誤範例'"))
            self.assertEqual(type(e), type(ValueError()))

        try:
            Slot(label="DATE", text="99:99:99")
        except Exception as e:
            self.assertEqual(type(e), type(ValueError()))
            self.assertEqual(str(e), str("UnboundLocalError: local variable 'month' referenced before assignment"))

    def test_SlotMoney(self):

        #
        try:
            Slot(label="MON", text="奇怪的金額")
        except Exception as e:
            self.assertEqual(str(e), str("KeyError: '額'"))
            self.assertEqual(type(e), type(ValueError()))

    def test_SlotCHA(self):
        #

        try:
            Slot(label="CHA", text="奇怪的渠道")
        except Exception as e:
            self.assertEqual(str(e), str("ValueError: 奇怪的渠道 is UNKNOWN"))
            self.assertEqual(type(e), type(ValueError()))

    def test_SlotTIME(self):
        #
        try:
            Slot(label="TIME", text="錯誤範例")
        except Exception as e:
            self.assertEqual(str(e), str("KeyError: '例'"))
            self.assertEqual(type(e), type(ValueError()))

        try:
            Slot(label="TIME", text="99:99:99")
        except Exception as e:
            self.assertEqual(type(e), type(ValueError()))
            self.assertEqual(str(e), str("ValueError: hour must be in 0..23"))

    def test_SlotPER(self):

        #
        try:
            Slot(label="PER", text="2222錯誤範例小時")
        except Exception as e:
            self.assertEqual(str(e), str("KeyError: '例'"))
            self.assertEqual(type(e), type(ValueError()))

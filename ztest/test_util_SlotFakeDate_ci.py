import logging
import os
import unittest

from FakeSlot import FakeSlotMoney,FakeSlotDate, FakeSlotCHA, FakeSlotPeriod
from slot import Slot


class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        pass

    def test_FakeSlotMoney(self):
        for _ in range(100):
            # make the fake testing date
            a = FakeSlotMoney()
            ans, words = a.value, a.text

            # decode
            b = Slot(label=a.label, text=words)
            b.decode()

            # compared
            self.assertEqual(first=ans,second=b.value)
            #b.show()

    def test_FakeSlotDate(self):
        for _ in range(100):
            # make the fake testing date
            a = FakeSlotDate()
            ans, words = a.value, a.text
            b = Slot(label=a.label, text=a.text)
            self.assertEqual(first=ans, second=b.value)

    def test_FakeSlotCHA(self):
        for _ in range(100):
            # make the fake testing date
            a = FakeSlotCHA()
            ans, words = a.value, a.text
            b = Slot(label=a.label, text=a.text)
            self.assertEqual(first=ans, second=b.value)

    def test_FakeSlotCHA(self):
        for _ in range(100):
            # make the fake testing date
            a = FakeSlotPeriod()
            ans, words = a.value, a.text
            #a.show()
            b = Slot(label=a.label, text=a.text)
            self.assertEqual(first=ans, second=b.value)
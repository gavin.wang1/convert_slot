import logging
import os
import unittest
import datetime
from datetime import date, timedelta, datetime
from pypinyin import pinyin
from FakeSlot import FakeSlotGold, FakeSlotMoney, Slot
from slot import Slot
from Decoder import PeriodDecoder
from pypinyin import pinyin, lazy_pinyin


class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        today = date.today() - timedelta(days=1)
        offset = (today.weekday() - 2) % 7
        last_wednesday = today - timedelta(days=offset)
        self.lastweek_3 = last_wednesday
        pass

    def loadfile(self,file_der="{}/test_csvDoc/cha.csv".format(os.path.dirname(os.path.abspath(__file__)))):

        arr = []
        with open(file_der, newline='') as f:
            rows = f.readlines()
            for r in rows:
                arr.append(r.split(","))
        return arr

    def test_SlotCHA(self):

        with open("./cha_test_result.csv", "w") as f:
            s = self.loadfile()
            f.write("{},{},{},{},{},{},{},{},{},{}\n".format("文句", "答案", "#", "編輯距離", "分數", "#", "發音編輯距離", "分數", "文句拼音", "答案拼音"))
            for row in s:
                ans = row[0]
                slot = row[1].strip("\n").strip("\r")
                a = Slot(label="CHA", text=slot, DEBUG=True)
                token = "O" if ans == a.value else "X" #if ans not in a.value else "v"
                f.write("{},{},{},{},{},".format(slot, ans, token, a.value, a.score))
                #f.write("{},原始字句:{},解析:{},正確答案:{},分數:{}".format(token, slot, a.value, ans, a.score))
                a = Slot(label="CHA2", text=slot, DEBUG=True)
                token = "O" if ans == a.value else "X" #if ans not in a.value else "v"
                f.write("{},{},{},{},{}\n".format(token, a.value, a.score, " ".join(lazy_pinyin(slot)), " ".join(lazy_pinyin(a.value))))
                #f.write("{},原始字句:{},解析:{},正確答案:{},分數:{}\n".format(token, slot, a.value, ans, a.score))
                #f.write("\n")



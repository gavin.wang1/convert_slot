import logging
import os
import unittest
import datetime
import csv
import re
from datetime import date, timedelta, datetime

from FakeSlot import FakeSlotGold, FakeSlotMoney
from slot import Slot

class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    @classmethod
    def setUp(self) -> None:
        self.localhost = os.path.dirname(os.path.abspath(__file__))
        output=[]
        # 開啟 CSV 檔案
        with open(f'{self.localhost}/5-fold-evaluation-predict.csv', newline='') as csvfile:
            # 讀取 CSV 檔案內容
            rows = csv.reader(csvfile,delimiter='\t')
            # 以迴圈輸出每一列
            for idx, row in enumerate(rows):
                if idx == 0: continue
                row[3] = re.sub(r"\[|\]| |\'|\"", '', row[3])
                row[4] = re.sub(r"\[|\]| |\'|\"", '', row[4])
                for label, value in zip(row[3].split(','),row[4].split(',')):
                    if [label,value] in [
                        ["時間", "年5月30日"],
                        ["日期", "155-9353-24"],
                        ["時間", "10分钟左右"],
                        ["時間", "右"],
                        ["時間", "03.73"],
                        ["金額", "K我他"],
                        ["時間", "三千九点三十八"],
                        ["時間", "个小时"],
                        ["金額", "。都"],
                        ["金額", "三点四十五一百元"],
                        ["日期", "15.28分"],
                        ["時間", "60八点二十七"],
                        ["日期", "15.28分钟"],
                        ["日期", "两个号"],
                        ["日期", "20"],
                        ["日期", "天"],
                        ["日期", "201905302000400111009700741100015月30号"],
                        ["金額", "支"],
                        ["時間", "上"],
                    ]: continue

                    try:
                        a = Slot(label=label,text=value)
                    except ValueError as e:
                        if "UNKNOWN" in str(e):
                            output.append([label, value, "UNKNOWN"])
                        else:
                            raise e
                    #print(label, value)
                    #a.show()
                    output.append([a.label, a.text, a.value])
                    #print("\t")
        # 開啟輸出的 CSV 檔案

        with open(f'{self.localhost}/output_test_util_loadcsv.csv', 'w', newline='') as csvfile:
            # 建立 CSV 檔寫入器
            writer = csv.writer(csvfile)

            # 寫入一列資料
            writer.writerow(["label", "text", "value"])
            saved = []
            for r in output:
                if r in saved: continue
                # 寫入另外幾列資料
                writer.writerow(r)
                saved.append(r)


    def test_SlotDate(self):
       pass
import logging
import os
import unittest
import datetime
from datetime import date, timedelta, datetime
import requests
import json

from FakeSlot import FakeSlotGold, FakeSlotMoney, Slot
from slot import Slot
from Decoder import PeriodDecoder

class TestSlotDecoderTool(unittest.TestCase):
    """Test Util Tool"""

    def call_api(self, text = "现在总该说明一下了把，72小时都过了，黄花菜都凉了，我的钱是个怎么回事"):

        header = {
            "token": "dadd031d4af39d04",
            "debug": "1"
        }
        request_reponds = requests.get("http://10.205.50.2/api/query?debug=1&q={}".format(text), headers=header)
        return json.loads(request_reponds.content)

    def check_entities(self,full_arr, entity_type, entity_text) -> bool:

        if entity_type not in full_arr: #沒有正確的類別
            return -1
        elif entity_text in full_arr[entity_type]:
            return 1
        elif entity_text not in full_arr[entity_type]:
            return 0

    def loadfile(self,file_der="{}/predict_test.csv".format(os.path.dirname(os.path.abspath(__file__)))):

        arr = []
        with open(file_der, newline='') as f:
            rows = f.readlines()
            for r in rows:
                arr.append(r.split(","))
        return arr

    def test_API_TEST(self):
        csv_sentence = self.loadfile()
        for s in csv_sentence:
            sentence = s[0]
            text = s[1]
            label = s[2]
            ans = s[3].strip("\n")
            orig_entities = self.call_api(text=sentence)['retStr']['orig entities']

            # check api
            predict_result = self.check_entities(orig_entities, label, text)
            if predict_result == -1: #沒有正確的類別
                print("WARRING:#沒有正確的類別 MODEL PREDICT: {},{}".format(orig_entities, sentence))

            elif predict_result == 1:
                # convert slot
                a = Slot(label=label, text=text)
                if str(a.value) == str(ans):
                    print("\tSUCCESS: MODEL PREDICT: {},{} / {}:{}".format(orig_entities[label], sentence, ans, a.value))
                    pass_token = True
                else:
                    print("ERROR: MODEL PREDICT: {},{} / {}:{} ".format(orig_entities[label], sentence, ans, a.value))

            elif predict_result == 0: #沒有完整的詞彙
                print("WARRING:#沒有完整的詞彙 MODEL PREDICT: {},{}".format(orig_entities, sentence))

